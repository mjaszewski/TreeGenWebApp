package pl.mj.treegen.core.tree.model.validation.between.integers;

import org.junit.BeforeClass;
import org.junit.Test;
import pl.mj.treegen.core.tree.model.range.RangeInteger;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static javax.validation.Validation.buildDefaultValidatorFactory;

/**
 * Created by Mateusz Jaszewski on 2016-01-25.
 */
public class BetweenIntegersValidatorTest {

    private class ValidBean {
        @BetweenIntegers(min = 1, max = 10)
        private RangeInteger validRange = new RangeInteger(1, 10);

        @BetweenIntegers(min = 1, max = 10)
        private int value = 5;
    }

    private class InvalidBean {
        @BetweenIntegers(min = 1, max = 10)
        private RangeInteger invalidRange = new RangeInteger(0, 11);

        @BetweenIntegers(min = 1, max = 10)
        private int value = 15;
    }

    private static Validator validator;

    @BeforeClass
    public static void initValidator() {
        ValidatorFactory factory = buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void shouldBeValid() {
        Set<ConstraintViolation<ValidBean>> constraintViolations = validator.validate(new ValidBean());
        assert constraintViolations.size() == 0;
    }

    @Test
    public void shouldBeInvalid() {
        Set<ConstraintViolation<InvalidBean>> constraintViolations = validator.validate(new InvalidBean());
        assert constraintViolations.size() == 2;
    }

}
