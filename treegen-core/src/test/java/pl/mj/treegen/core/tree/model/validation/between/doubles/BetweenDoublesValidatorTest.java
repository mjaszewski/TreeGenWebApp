package pl.mj.treegen.core.tree.model.validation.between.doubles;

import org.junit.BeforeClass;
import org.junit.Test;
import pl.mj.treegen.core.tree.model.range.RangeDouble;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static javax.validation.Validation.buildDefaultValidatorFactory;

/**
 * Created by Mateusz Jaszewski on 2016-01-25.
 */
public class BetweenDoublesValidatorTest {

    private class ValidBean {
        @BetweenDoubles(min = 0, max = 1)
        private RangeDouble validRange = new RangeDouble(0.0, 1.0);

        @BetweenDoubles(min = 0, max = 1)
        private double value = 0.5;
    }

    private class InvalidBean {
        @BetweenDoubles(min = 0, max = 1)
        private RangeDouble invalidRange = new RangeDouble(0.0, 1.1);


        @BetweenDoubles(min = 0, max = 1)
        private double value = 1.5;
    }

    private static Validator validator;

    @BeforeClass
    public static void initValidator() {
        ValidatorFactory factory = buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void shouldBeValid() {
        Set<ConstraintViolation<ValidBean>> constraintViolations = validator.validate(new ValidBean());
        assert constraintViolations.size() == 0;
    }

    @Test
    public void shouldBeInvalid() {
        Set<ConstraintViolation<InvalidBean>> constraintViolations = validator.validate(new InvalidBean());
        assert constraintViolations.size() == 2;
    }

}
