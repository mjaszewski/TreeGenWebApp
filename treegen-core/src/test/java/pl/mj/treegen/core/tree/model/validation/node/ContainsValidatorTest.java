package pl.mj.treegen.core.tree.model.validation.node;

import org.junit.BeforeClass;
import org.junit.Test;
import pl.mj.treegen.core.tree.model.nodes.implementations.RootsNode;
import pl.mj.treegen.core.tree.model.nodes.implementations.TreeNode;
import pl.mj.treegen.core.tree.model.nodes.implementations.TrunkNode;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static javax.validation.Validation.buildDefaultValidatorFactory;

/**
 * Created by Mateusz Jaszewski on 2016-01-25.
 */
public class ContainsValidatorTest {
    private static Validator validator;

    @BeforeClass
    public static void initValidator() {
        ValidatorFactory factory = buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void shouldBeValid() {
        TreeNode treeNode = new TreeNode();
        treeNode.getSubNodes().add(new TrunkNode());
        treeNode.getSubNodes().add(new RootsNode());

        Set<ConstraintViolation<TreeNode>> constraintViolations = validator.validate(treeNode);
        assert constraintViolations.size() == 0;
    }

    @Test
    public void shouldBeInvalid() {
        TreeNode treeNode = new TreeNode();
        treeNode.getSubNodes().add(new TrunkNode());
        treeNode.getSubNodes().add(new TrunkNode());

        Set<ConstraintViolation<TreeNode>> constraintViolations = validator.validate(treeNode);
        assert constraintViolations.size() == 1;
    }
}
