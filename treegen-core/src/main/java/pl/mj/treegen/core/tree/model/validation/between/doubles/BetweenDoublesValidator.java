package pl.mj.treegen.core.tree.model.validation.between.doubles;

import pl.mj.treegen.core.tree.model.range.RangeDouble;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Mateusz Jaszewski on 2016-01-24.
 */
public class BetweenDoublesValidator implements ConstraintValidator<BetweenDoubles, Object> {

    private double min;
    private double max;

    @Override
    public void initialize(BetweenDoubles constraintAnnotation) {
        this.min = constraintAnnotation.min();
        this.max = constraintAnnotation.max();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        boolean isValid = false;

        if (value instanceof Double) {
            double valueDouble = (double) value;
            isValid = valueDouble >= min && valueDouble <= max;
        }
        if (value instanceof RangeDouble) {
            RangeDouble rangeDouble = (RangeDouble) value;
            isValid = rangeDouble.getMin() >= min && rangeDouble.getMax() <= max;
        }
        return isValid;
    }
}
