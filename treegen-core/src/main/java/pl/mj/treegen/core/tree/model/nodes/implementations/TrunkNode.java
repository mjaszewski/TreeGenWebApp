package pl.mj.treegen.core.tree.model.nodes.implementations;

import lombok.Data;
import pl.mj.treegen.core.tree.model.nodes.abstarct.Node;
import pl.mj.treegen.core.tree.model.nodes.abstarct.NodeWithSubNodes;
import pl.mj.treegen.core.tree.model.range.RangeDouble;
import pl.mj.treegen.core.tree.model.range.RangeInteger;
import pl.mj.treegen.core.tree.model.validation.between.doubles.BetweenDoubles;
import pl.mj.treegen.core.tree.model.validation.node.Contains;
import pl.mj.treegen.core.tree.model.validation.node.SubNodes;

import java.util.ArrayList;
import java.util.List;

@Data
public class TrunkNode implements NodeWithSubNodes {

	@SubNodes({
			@Contains(subNode = TreeNode.class, maxCount = 0),
			@Contains(subNode = TrunkNode.class, maxCount = 0),
			@Contains(subNode = RootsNode.class, maxCount = 0),
			@Contains(subNode = LeavesNode.class, maxCount = 0),
			@Contains(subNode = BranchesNode.class, maxCount = 1),
			@Contains(subNode = SideBranchesNode.class, maxCount = 1)
	})
	private List<Node> subNodes = new ArrayList<>();

	@BetweenDoubles(min = 0, max = 3)
	private double shape = 1;

	@BetweenDoubles(min = 0, max = 1)
	private double roughness = 0.5;

	@BetweenDoubles(min = 0, max = 1)
	private RangeDouble bendingStrength = new RangeDouble(0.0, 1.0);

	@BetweenDoubles(min = 1, max = 20)
	private RangeInteger numberOfBends = new RangeInteger(9, 11);

	@Override
	public List<Node> getSubNodes() {
		return subNodes;
	}

	@Override
	public void setSubNodes(List<Node> subNodes) {
		this.subNodes = subNodes;
	}
}
