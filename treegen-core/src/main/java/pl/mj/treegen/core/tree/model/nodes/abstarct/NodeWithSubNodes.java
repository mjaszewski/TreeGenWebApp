package pl.mj.treegen.core.tree.model.nodes.abstarct;

import java.util.List;

/**
 * Created by Mateusz Jaszewski on 2016-01-17.
 */
public interface NodeWithSubNodes extends Node {
    List<Node> getSubNodes();
    void setSubNodes(List<Node> subNodes);
}
