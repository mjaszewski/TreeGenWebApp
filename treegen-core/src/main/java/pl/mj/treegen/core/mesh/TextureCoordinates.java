package pl.mj.treegen.core.mesh;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Mateusz Jaszewski on 2016-01-16.
 */
@Data
@AllArgsConstructor
public class TextureCoordinates {
    private double u;
    private double v;

    @Override
    public String toString() {
        return u + " " + v;
    }
}
