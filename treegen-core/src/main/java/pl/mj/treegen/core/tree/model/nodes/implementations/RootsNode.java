package pl.mj.treegen.core.tree.model.nodes.implementations;

import lombok.Data;
import pl.mj.treegen.core.tree.model.nodes.abstarct.Node;
import pl.mj.treegen.core.tree.model.range.RangeDouble;
import pl.mj.treegen.core.tree.model.range.RangeInteger;
import pl.mj.treegen.core.tree.model.validation.between.doubles.BetweenDoubles;
import pl.mj.treegen.core.tree.model.validation.between.integers.BetweenIntegers;

@Data
public class RootsNode implements Node {

	@BetweenDoubles(min = 0, max = 3)
	private double shape = 1;

	@BetweenIntegers(min = 2, max = 20)
	private int number = 9;

	@BetweenDoubles(min = 0, max = 90)
	private double startDirection = 45;

	@BetweenDoubles(min = 0, max = 90)
	private double endDirection = 45;

	@BetweenDoubles(min = 0, max = 1)
	private double roughness = 0.5;

	@BetweenDoubles(min = 1, max = 100)
	private RangeDouble length = new RangeDouble(35.0, 50.0);

	@BetweenDoubles(min = 0, max = 1)
	private RangeDouble radius = new RangeDouble(0.0, 1.0);

	@BetweenDoubles(min = 0, max = 1)
	private RangeDouble bendingStrength = new RangeDouble(0.0, 1.0);

	@BetweenIntegers(min = 1, max = 10)
	private RangeInteger numberOfBends = new RangeInteger(4, 5);

}
