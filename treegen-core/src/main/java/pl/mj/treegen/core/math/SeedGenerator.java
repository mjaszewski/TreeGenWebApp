package pl.mj.treegen.core.math;

public class SeedGenerator {
    private int counter = 0;

    public void reset() {
        counter = 0;
    }

    public void reset(int startValue) {
        counter = startValue;
    }

    public int nextSeed() {
        int seed = counter;
        counter++;
        return seed;
    }
}
