package pl.mj.treegen.core.math;

public class Matrix {
    private double[][] elements;

    public Matrix() {
        elements = new double[4][4];
        identity();
    }

    public double get(int row, int column) {
        return elements[row][column];
    }

    public void set(int row, int column, double value) {
        elements[row][column] = value;
    }

    static public Matrix mult(final Matrix a, final Matrix b) {
        Matrix matrix = new Matrix();
        double sum;
        for (int row = 0; row < 4; row++) {
            for (int column = 0; column < 4; column++) {
                sum = 0.0;
                for (int i = 0; i < 4; i++) {
                    sum += a.get(row, i) * b.get(i, column);
                }
                matrix.elements[row][column] = sum;
            }
        }
        return matrix;
    }

    static public Vector3 mult(final Vector3 vector, final Matrix matrix) {
        double x, y, z;
        x = matrix.get(0, 0) * vector.getX() + matrix.get(1, 0) * vector.getY() + matrix.get(2, 0) * vector.getZ();
        y = matrix.get(0, 1) * vector.getX() + matrix.get(1, 1) * vector.getY() + matrix.get(2, 1) * vector.getZ();
        z = matrix.get(0, 2) * vector.getX() + matrix.get(1, 2) * vector.getY() + matrix.get(2, 2) * vector.getZ();
        return new Vector3(x, y, z);
    }

    public void identity() {
        for (int row = 0; row < 4; row++) {
            for (int column = 0; column < 4; column++) {
                elements[row][column] = 0.0;
            }
        }
        for (int i = 0; i < 4; i++) {
            elements[i][i] = 1.0;
        }
    }


    public void rotate(double angle, Vector3 axis) {
        identity();
        elements[0][0] = Math.cos(angle) + axis.getX() * axis.getX() * (1.0 -  Math.cos(angle));
        elements[0][1] = axis.getX() * axis.getY() * (1.0 -  Math.cos(angle)) - axis.getZ() *  Math.sin(angle);
        elements[0][2] = axis.getX() * axis.getZ() * (1.0 -  Math.cos(angle)) + axis.getY() *  Math.sin(angle);

        elements[1][0] = axis.getY() * axis.getX() * (1.0 -  Math.cos(angle)) + axis.getZ() *  Math.sin(angle);
        elements[1][1] =  Math.cos(angle) + axis.getY() * axis.getY() * (1 -  Math.cos(angle));
        elements[1][2] = axis.getY() * axis.getZ() * (1.0 -  Math.cos(angle)) - axis.getX() *  Math.sin(angle);

        elements[2][0] = axis.getZ() * axis.getX() * (1.0 -  Math.cos(angle)) - axis.getY() *  Math.sin(angle);
        elements[2][1] = axis.getZ() * axis.getY() * (1.0 -  Math.cos(angle)) + axis.getX() *  Math.sin(angle);
        elements[2][2] =  Math.cos(angle) + axis.getZ() * axis.getZ() * (1.0 -  Math.cos(angle));
    }

    public void rotateX(double angle) {
        identity();
        elements[1][1] =  Math.cos(angle);
        elements[2][2] =  Math.cos(angle);
        elements[1][2] =  Math.sin(angle);
        elements[2][1] = - Math.sin(angle);
    }

    public void rotateY(double angle) {
        identity();
        elements[0][0] =  Math.cos(angle);
        elements[2][2] =  Math.cos(angle);
        elements[0][2] = - Math.sin(angle);
        elements[2][0] =  Math.sin(angle);
    }

    public void rotateZ(double angle) {
        identity();
        elements[0][0] =  Math.cos(angle);
        elements[1][1] =  Math.cos(angle);
        elements[0][1] =  Math.sin(angle);
        elements[1][0] = - Math.sin(angle);
    }


    public void translate(Vector3 vector) {
        identity();
        elements[0][3] = vector.getX();
        elements[1][3] = vector.getY();
        elements[2][3] = vector.getZ();
    }

    public void scale(Vector3 vector) {
        identity();
        elements[0][0] = vector.getX();
        elements[1][1] = vector.getY();
        elements[2][2] = vector.getZ();
    }

    public String toString() {
        String r = "";
        for (int i = 0; i < 4; i++) {
            r = r + "[ ";
            for (int j = 0; j < 4; j++)
                r = r + elements[i][j] + " ";
            r = r + "] ";
        }
        return r;
    }

}
