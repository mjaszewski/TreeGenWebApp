package pl.mj.treegen.core;

import pl.mj.treegen.core.exporter.Exporter;
import pl.mj.treegen.core.exporter.implementations.OBJExporter;
import pl.mj.treegen.core.mesh.Mesh;
import pl.mj.treegen.core.tree.generator.Tree;
import pl.mj.treegen.core.tree.generator.lod.LevelOfDetails;
import pl.mj.treegen.core.tree.model.nodes.implementations.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by Mateusz Jaszewski on 2016-01-24.
 */
public class Main {
    public static void main(String[] args) throws IOException {

        TreeNode treeNode = new TreeNode();
        treeNode.getSubNodes().add(new LeavesNode());
        treeNode.getSubNodes().add(new RootsNode());

        BranchesNode branchesNode = new BranchesNode();
        branchesNode.getSubNodes().add(new SideBranchesNode());

        TrunkNode trunkNode = new TrunkNode();
        trunkNode.getSubNodes().add(branchesNode);

        treeNode.getSubNodes().add(trunkNode);

        Tree tree = new Tree(treeNode, 2454);

        Mesh mesh = tree.generateTreeMesh(new LevelOfDetails(3, 5));
        Mesh leavesMesh = tree.generateLeavesMesh();

        FileOutputStream fileOutputStream = new FileOutputStream(new File("test.obj"));
        Exporter exporter = new OBJExporter();

        exporter.export(mesh, leavesMesh, fileOutputStream);
    }
}
