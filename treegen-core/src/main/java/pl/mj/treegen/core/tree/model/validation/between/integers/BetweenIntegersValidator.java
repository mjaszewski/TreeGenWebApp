package pl.mj.treegen.core.tree.model.validation.between.integers;

import pl.mj.treegen.core.tree.model.range.RangeInteger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Mateusz Jaszewski on 2016-01-24.
 */
public class BetweenIntegersValidator implements ConstraintValidator<BetweenIntegers, Object> {

    private int min;
    private int max;

    @Override
    public void initialize(BetweenIntegers constraintAnnotation) {
        this.min = constraintAnnotation.min();
        this.max = constraintAnnotation.max();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        boolean isValid = false;

        if (value instanceof Integer) {
            int valueInt = (int) value;
            isValid = valueInt >= min && valueInt <= max;
        }
        if (value instanceof RangeInteger) {
            RangeInteger rangeInteger = (RangeInteger) value;
            isValid = rangeInteger.getMin() >= min && rangeInteger.getMax() <= max;
        }
        return isValid;
    }
}
