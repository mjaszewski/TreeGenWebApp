package pl.mj.treegen.core.mesh;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.mj.treegen.core.math.Vector3;

/**
 * Created by Mateusz Jaszewski on 2016-01-16.
 */

@Data
@AllArgsConstructor
public class Vertex{
    private Vector3 position;
    private Vector3 normal;
}