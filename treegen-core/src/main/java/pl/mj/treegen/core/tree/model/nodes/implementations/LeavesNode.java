package pl.mj.treegen.core.tree.model.nodes.implementations;

import lombok.Data;
import pl.mj.treegen.core.tree.model.nodes.abstarct.Node;
import pl.mj.treegen.core.tree.model.range.RangeDouble;
import pl.mj.treegen.core.tree.model.validation.between.doubles.BetweenDoubles;
import pl.mj.treegen.core.tree.generator.leaves.LeavesType;

import javax.validation.constraints.NotNull;

import static pl.mj.treegen.core.tree.generator.leaves.LeavesType.*;

@Data
public class LeavesNode implements Node {

	@BetweenDoubles(min = 0, max = 20)
	private RangeDouble size = new RangeDouble(10.0, 15.0);

	@NotNull
	private LeavesType leavesType = TRIPLE;
}
