package pl.mj.treegen.core.tree.model.range;

import pl.mj.treegen.core.math.RandomGenerator;

/**
 * Created by Mateusz Jaszewski on 2016-01-19.
 */
public class RangeInteger extends Range<Integer> {

    public RangeInteger(Integer min, Integer max) {
        super(min, max);
    }

    @Override
    public Integer random(RandomGenerator randomGenerator, int seed) {
        return randomGenerator.randomInt(min, max, seed);
    }
}
