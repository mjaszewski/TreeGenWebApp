package pl.mj.treegen.core.math;

public class Vector3 {
    private double x;
    private double y;
    private double z;

    public Vector3() {
        x = 0f;
        y = 0f;
        z = 0f;
    }

    public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static Vector3 add(Vector3 a, Vector3 b) {
        return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static Vector3 sub(Vector3 a, Vector3 b) {
        return new Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    public static Vector3 cross(Vector3 a, Vector3 b) {
        return new Vector3(a.y * b.z - a.z * b.y,
                a.z * b.x - a.x * b.z,
                a.x * b.y - a.y * b.x);
    }

    public static Vector3 mult(Vector3 v, double f) {
        return new Vector3(v.x * f, v.y * f, v.z * f);
    }

    public Vector3 negative() {
        Vector3 n = new Vector3(-x, -y, -z);
        return n;
    }

    public Vector3 anyNormal() {
        Vector3 v = new Vector3(y, z, x);
        return Vector3.cross(this, v);
    }

    public double length() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    public void normalize() {
        double l = length();
        x = x / l;
        y = y / l;
        z = z / l;
    }

    public void mult(double f) {
        x *= f;
        y *= f;
        z *= f;
    }

    public void add(Vector3 v) {
        x += v.x;
        y += v.y;
        z += v.z;
    }

    public void sub(Vector3 v) {
        x -= v.x;
        y -= v.y;
        z -= v.z;
    }

    public double getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public String toString() {
        return x + " " + y + " " + z;
    }

    public Vector3 clone() {
        Vector3 v = new Vector3();
        v.x = x;
        v.y = y;
        v.z = z;
        return v;
    }
}
