package pl.mj.treegen.core.tree.model.nodes.implementations;

import lombok.Data;
import pl.mj.treegen.core.tree.model.nodes.abstarct.Node;
import pl.mj.treegen.core.tree.model.nodes.abstarct.NodeWithSubNodes;
import pl.mj.treegen.core.tree.model.validation.between.doubles.BetweenDoubles;
import pl.mj.treegen.core.tree.model.validation.node.Contains;
import pl.mj.treegen.core.tree.model.validation.node.SubNodes;

import java.util.ArrayList;
import java.util.List;

@Data
public class TreeNode implements NodeWithSubNodes {

	@SubNodes({
			@Contains(subNode = TreeNode.class, maxCount = 0),
			@Contains(subNode = BranchesNode.class, maxCount = 0),
			@Contains(subNode = SideBranchesNode.class, maxCount = 0),
			@Contains(subNode = TrunkNode.class, maxCount = 1),
			@Contains(subNode = RootsNode.class, maxCount = 1),
			@Contains(subNode = LeavesNode.class, maxCount = 1)
	})
	private List<Node> subNodes = new ArrayList<>();

	@BetweenDoubles(min = 0, max = 200)
	private double height = 200;

	@BetweenDoubles(min = 0, max = 10)
	private double radius = 5;

	@BetweenDoubles(min = 0, max = 10)
	private double textureScaleU = 1;

	@BetweenDoubles(min = 0, max = 10)
	private double textureScaleV = 1;

	@Override
	public List<Node> getSubNodes() {
		return subNodes;
	}

	@Override
	public void setSubNodes(List<Node> subNodes) {
		this.subNodes = subNodes;
	}
}
