package pl.mj.treegen.core.exporter;

import pl.mj.treegen.core.mesh.Mesh;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Mateusz Jaszewski on 2016-01-27.
 */
public interface Exporter {
    void export(Mesh treeMesh, Mesh leavesMesh, OutputStream outputStream) throws IOException;
}
