package pl.mj.treegen.core.tree.generator;

import pl.mj.treegen.core.math.Matrix;
import pl.mj.treegen.core.math.RandomGenerator;
import pl.mj.treegen.core.math.SeedGenerator;
import pl.mj.treegen.core.math.Vector3;
import pl.mj.treegen.core.mesh.Mesh;
import pl.mj.treegen.core.tree.generator.lod.LevelOfDetails;
import pl.mj.treegen.core.tree.generator.treeelement.TreeElement;
import pl.mj.treegen.core.tree.generator.treeelement.TreeElementPoint;
import pl.mj.treegen.core.tree.model.nodes.abstarct.Node;
import pl.mj.treegen.core.tree.model.nodes.abstarct.NodeWithSubNodes;
import pl.mj.treegen.core.tree.model.nodes.implementations.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static pl.mj.treegen.core.math.Vector3.*;
import static pl.mj.treegen.core.mesh.Mesh.regroup;
import static pl.mj.treegen.core.tree.generator.leaves.Leaves.generateLeaves;

public class Tree {
    public static final float MAGIC_NUMBER = 137.5f;
    private List<TreeElement> elements;

    private TreeNode treeNode;
    private TrunkNode trunkNode;
    private RootsNode rootsNode;
    private LeavesNode leavesNode;

    private SeedGenerator seedGenerator;
    private RandomGenerator randomGenerator;

    public Tree(TreeNode treeNode, int seed) {
        this.treeNode = treeNode;

        seedGenerator = new SeedGenerator();
        randomGenerator = new RandomGenerator(seed);
        elements = new ArrayList<>();

        findSubNodes();
        generateTree(treeNode);
    }

    public Mesh generateTreeMesh(LevelOfDetails levelOfDetails) {
        return regroup(elements.stream()
                .map(element -> element.generateMesh(treeNode.getTextureScaleU(), treeNode.getTextureScaleV(), levelOfDetails))
                .collect(toList()));
    }

    public Mesh generateLeavesMesh() {
        if (leavesNode != null) {
            List<TreeElementPoint> ends = elements.stream()
                    .filter(element -> element.getHasLeaves())
                    .map(TreeElement::getLastPoint)
                    .collect(toList());
            return generateLeaves(leavesNode, ends, randomGenerator, seedGenerator);
        }
        //FIXME: zwracanie pustego mesh'a
        return new Mesh();
    }

    private void findSubNodes() {
        treeNode.getSubNodes().forEach(subNode -> {
            if (subNode instanceof TrunkNode) {
                trunkNode = (TrunkNode) subNode;
            }
            if (subNode instanceof RootsNode) {
                rootsNode = (RootsNode) subNode;
            }
            if (subNode instanceof LeavesNode) {
                leavesNode = (LeavesNode) subNode;
            }
        });
    }

    private void generateTree(TreeNode treeNode) {
        elements.clear();
        if (trunkNode != null) {
            TreeElement trunk = new TreeElement(randomGenerator, seedGenerator.nextSeed());
            trunk.generateTrunk(treeNode, trunkNode);
            elements.add(trunk);

            if (rootsNode != null) {
                TreeElementPoint startPoint = trunk.approximatePoint(0).clone();
                generateRoots(rootsNode, startPoint);
            }
            generateTreeElements(trunkNode, Arrays.asList(trunk));
        }
    }

    private void generateRoots(RootsNode rootsNode, TreeElementPoint startPoint) {
        int number = rootsNode.getNumber();
        double startDirectionParameter = rootsNode.getStartDirection();
        double endDirectionParameter = rootsNode.getEndDirection();

        TreeElement root;
        for (int i = 0; i < number; i++) {

            Matrix matrix = new Matrix();

            matrix.rotate(Math.toRadians(startDirectionParameter), new Vector3(0, 0, 1));
            Vector3 startDirection = Matrix.mult(new Vector3(1, 0, 0), matrix);

            double angle = i / number * 360.0;
            matrix.rotate(Math.toRadians(angle), new Vector3(0, 1, 0));
            startDirection = Matrix.mult(startDirection, matrix);
            startDirection.normalize();

            matrix.rotate(Math.toRadians(90 - endDirectionParameter), new Vector3(0, 0, 1));
            Vector3 endDirection = Matrix.mult(new Vector3(1, 0, 0), matrix);

            angle = i / number * 360.0;
            matrix.rotate(Math.toRadians(angle), new Vector3(0, 1, 0));
            endDirection = Matrix.mult(endDirection, matrix);

            endDirection.normalize();

            root = new TreeElement(randomGenerator, seedGenerator.nextSeed());
            root.generateRoot(rootsNode, startPoint, startDirection, endDirection);
            elements.add(root);
        }
    }

    private List<TreeElement> generateBranches(BranchesNode branchesNode, List<TreeElement> list) {
        List<TreeElement> addedTreeElements = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {

            int numberOfBranches = branchesNode.getNumber().random(randomGenerator, seedGenerator.nextSeed());
            double spread = branchesNode.getSpread().random(randomGenerator, seedGenerator.nextSeed());

            TreeElementPoint startPoint = list.get(i).getLastPoint();

            List<Vector3> startDirections = new ArrayList<>();

            Vector3 normal = cross(startPoint.getDirection(), new Vector3(1, 0, 0));
            normal.normalize();
            Vector3 startDirection = add(mult(normal, spread), mult(startPoint.getDirection(), 1f - spread));
            startDirection.normalize();

            double startAngle = randomGenerator.randomDouble(0, 180, seedGenerator.nextSeed());
            for (int j = 0; j < numberOfBranches; j++) {
                Vector3 nextStartDirection;
                Matrix rotation = new Matrix();
                rotation.rotate(Math.toRadians(startAngle + j / numberOfBranches * 360f), startPoint.getDirection());
                nextStartDirection = Matrix.mult(startDirection, rotation);
                startDirections.add(nextStartDirection);
            }

            for (int j = 0; j < numberOfBranches; j++) {
                TreeElement branch = new TreeElement(randomGenerator, seedGenerator.nextSeed());
                branch.generateBranch(branchesNode, startPoint, startDirections.get(j), list.get(i));
                elements.add(branch);
                addedTreeElements.add(branch);
            }
        }
        return addedTreeElements;
    }

    private List<TreeElement> generateSideBranches(SideBranchesNode sideBranchesNode, List<TreeElement> list) {
        List<TreeElement> addedTreeElements = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            int numberOfBranches = sideBranchesNode.getNumber().random(randomGenerator, seedGenerator.nextSeed());
            double start = sideBranchesNode.getStart();
            double end = sideBranchesNode.getEnd();

            for (int j = 0; j < numberOfBranches; j++) {
                double position = (end - start) * (j / (double)numberOfBranches) + start;
                double rotation = j * MAGIC_NUMBER;

                TreeElement sideBranch = new TreeElement(randomGenerator, seedGenerator.nextSeed());
                sideBranch.generateSideBranch(sideBranchesNode, position, rotation, list.get(i));

                elements.add(sideBranch);
                addedTreeElements.add(sideBranch);
            }
        }
        return addedTreeElements;
    }

    private void generateTreeElements(Node node, List<TreeElement> list) {
        if(!(node instanceof NodeWithSubNodes)) {
            return;
        }
        ((NodeWithSubNodes)node).getSubNodes().forEach(subNode -> {
            List<TreeElement> addedTreeElements = null;
            if (subNode instanceof BranchesNode) {
                addedTreeElements = generateBranches((BranchesNode) subNode, list);
            }
            if (subNode instanceof SideBranchesNode) {
                addedTreeElements = generateSideBranches((SideBranchesNode) subNode, list);
            }
            generateTreeElements(subNode, addedTreeElements);
        });
    }

}
