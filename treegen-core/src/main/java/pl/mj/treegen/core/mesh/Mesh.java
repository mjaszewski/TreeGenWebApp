package pl.mj.treegen.core.mesh;

import java.util.ArrayList;
import java.util.List;

public class Mesh {

    public static Mesh regroup(List<Mesh> meshList) {
        int index = 0;
        Mesh regroupedMesh = new Mesh();

        for (Mesh mesh : meshList) {
            mesh.vertices.forEach(regroupedMesh::addVertex);

            for (Triangle t : mesh.triangles) {
                Triangle nt = new Triangle(t.getVertexIndex(0) + index,
                        t.getVertexIndex(1) + index,
                        t.getVertexIndex(2) + index);
                nt.setTextureCoordinatesArray(t.getTextureCoordinatesArray());
                regroupedMesh.addTriangle(nt);
            }

            index += mesh.vertices.size();
        }
        return regroupedMesh;
    }

    private List<Vertex> vertices = new ArrayList<>();
    private List<Triangle> triangles = new ArrayList<>();

    public void addVertex(Vertex vertex) {
        vertices.add(vertex);
    }

    public Vertex getVertex(int index) {
        return vertices.get(index);
    }

    public int countVertices() {
        return vertices.size();
    }

    public void addTriangle(Triangle triangle) {
        triangles.add(triangle);
    }

    public Triangle getTriangle(int index) {
        return triangles.get(index);
    }

    public int countTriangles() {
        return triangles.size();
    }

    public List<Vertex> getVertices() {
        return vertices;
    }

    public List<Triangle> getTriangles() {
        return triangles;
    }
}
