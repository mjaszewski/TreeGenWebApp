package pl.mj.treegen.core.tree.generator.treeelement;

import pl.mj.treegen.core.math.Matrix;
import pl.mj.treegen.core.math.RandomGenerator;
import pl.mj.treegen.core.math.SeedGenerator;
import pl.mj.treegen.core.math.Vector3;
import pl.mj.treegen.core.mesh.Mesh;
import pl.mj.treegen.core.mesh.TextureCoordinates;
import pl.mj.treegen.core.mesh.Triangle;
import pl.mj.treegen.core.mesh.Vertex;
import pl.mj.treegen.core.tree.generator.lod.LevelOfDetails;
import pl.mj.treegen.core.tree.model.nodes.implementations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.lang.Math.*;
import static pl.mj.treegen.core.math.Vector3.*;

public class TreeElement {

    private static final double POINT_DISTANCE = 2f;
    private static final int MAX_NUMBER_OF_BENDS = 20;

    private List<TreeElementPoint> points = new ArrayList<>();
    private boolean hasLeaves = false;

    private RandomGenerator randomGenerator;
    private SeedGenerator seedGenerator;

    private int seed;

    public static TreeElementPoint approximatePoint(double x, List<TreeElementPoint> points) {
        if (points.size() == 1) {
            return points.get(0);
        }
        TreeElementPoint point = new TreeElementPoint();

        int index1 = (int) (floor(x * (points.size() - 1)));
        int index2 = index1 + 1;
        if (index2 >= points.size())
            index2--;

        TreeElementPoint point1 = points.get(index1);
        TreeElementPoint point2 = points.get(index2);

        double x1 =  index1 / (double) (points.size() - 1);
        double x2 =  index2 / (double) (points.size() - 1);

        double d = x2 - x1;

        double d1 = (x - x1) / d;
        double d2 = (x2 - x) / d;

        point.setRadius(point1.getRadius() * d2 + point2.getRadius() * d1);
        point.setDirection(add(mult(point1.getDirection(), d2), mult(point2.getDirection(), d1)));
        point.setPosition(add(mult(point1.getPosition(), d2), mult(point2.getPosition(), d1)));
        for (int i = 0; i < TreeElementPoint.ARRAY_SIZE; i++) {
            point.setRadiusRandom(i, point1.getRadiusRandom(i) * d2 + point2.getRadiusRandom(i) * d1);
        }
        return point;
    }

    public static double distance(int start, int end, List<TreeElementPoint> points) {
        double distance = 0;
        Vector3 vector;
        for (int i = start; i < end; i++) {
            vector = sub(points.get(i + 1).getPosition(), points.get(i).getPosition());
            distance += vector.length();
        }
        return distance;
    }

    public TreeElement(RandomGenerator randomGenerator, int seed) {
        this.seed = seed;
        this.randomGenerator = randomGenerator;
        seedGenerator = new SeedGenerator();
        seedGenerator.reset(seed);
    }

    public double getRadius(double x) {
        return approximatePoint(x, points).getRadius();
    }

    public Vector3 getDirection(double x) {
        return approximatePoint(x, points).getDirection();
    }

    public Vector3 getPosition(double x) {
        return approximatePoint(x, points).getPosition();
    }

    public double length() {
        double length = 0;
        Vector3 vector;
        for (int i = 0; i < points.size() - 1; i++) {
            vector = sub(points.get(i + 1).getPosition(), points.get(i).getPosition());
            length += vector.length();
        }
        return length;
    }

    private List<Vector3> generateBends(RandomGenerator randomGenerator, SeedGenerator seedGenerator) {
        List<Vector3> list = new ArrayList<>();
        for (int i = 0; i < MAX_NUMBER_OF_BENDS; i++) {
            Vector3 direction = new Vector3(
                    randomGenerator.randomDouble(-1, 1, seedGenerator.nextSeed()),
                    randomGenerator.randomDouble(-1, 1, seedGenerator.nextSeed()),
                    randomGenerator.randomDouble(-1, 1, seedGenerator.nextSeed()));
            direction.normalize();
            list.add(direction);
        }
        return list;
    }

    private Vector3 direction(List<Vector3> bends, double x, int numberOfBends, double bendingStrength, Vector3 startDirection, double gravity) {
        if (numberOfBends < 2) {
            return startDirection;
        }

        if (x > 1) {
            x = 1;
        }

        int i = (int) (x * (numberOfBends - 1));
        double p = x - i * (1f / (numberOfBends - 1));
        p = p / (1f / (numberOfBends - 1));
        Vector3 v = add(mult(bends.get(i), 1f - p), mult(bends.get(i + 1), p));
        v.normalize();

        double strength = sqrt(x);

        Vector3 ret = add(mult(v, bendingStrength * strength * 0.7f), mult(startDirection, 1f - bendingStrength * strength * 0.5f));
        ret.normalize();

        ret = add(new Vector3(0, -x * gravity * 2, 0), ret);
        ret.normalize();
        return ret;
    }

    public void generateTrunk(TreeNode treeNode, TrunkNode trunkNode) {
        points.clear();
        hasLeaves = true;

        double radius = treeNode.getRadius();
        double height = treeNode.getHeight();
        double shape = trunkNode.getShape();
        double bendingStrength = trunkNode.getBendingStrength().random(randomGenerator, seed);
        int numberOfBends = trunkNode.getNumberOfBends().random(randomGenerator, seed);
        double roughness = trunkNode.getRoughness();

        double branchesStart = 1;
        Optional<BranchesNode> branchesNodeOptional = trunkNode.getSubNodes()
                .stream()
                    .filter(node -> node instanceof BranchesNode)
                    .map(node -> (BranchesNode)node)
                .findAny();

        if(branchesNodeOptional.isPresent()) {
            branchesStart = branchesNodeOptional.get().getStart();
            hasLeaves = false;
        }

        List<Vector3> bends = generateBends(randomGenerator, seedGenerator);

        Vector3 up = new Vector3(0, 1, 0);
        Vector3 position = new Vector3(0, 0, 0);
        Vector3 direction = new Vector3(0, 1, 0);

        int numberOfSteps = (int) floor(height / POINT_DISTANCE);
        double step = height;
        if (numberOfSteps == 0) {
            numberOfSteps = 1;
        } else {
            step = height / numberOfSteps;
        }

        List<TreeElementPoint> tmpPoints = new ArrayList<>();

        for (int i = 0; i <= numberOfSteps + 1; i++) {
            double currentRadius = (1f - pow(i * step / height, shape)) * radius;
            tmpPoints.add(new TreeElementPoint(position, direction, currentRadius, roughness, randomGenerator, points.size()));
            direction = direction(bends, i * step / (height * branchesStart), numberOfBends, bendingStrength, up, 0f);
            direction.mult(step);
            position = add(position, direction);
        }

        for (int i = 0; i < branchesStart * (tmpPoints.size() - 1); i++) {
            points.add(tmpPoints.get(i));
        }

        if (points.size() == 0) {
            points.add(tmpPoints.get(0));
        }
        if (branchesStart < 0.999f) {
            points.add(approximatePoint(branchesStart, tmpPoints));
        }
    }

    public void generateBranch(BranchesNode branchesNode, TreeElementPoint startPoint, Vector3 startDirection, TreeElement parentElement) {
        points.clear();
        points.add(startPoint);

        hasLeaves = true;

        double nextBranchesStart = 1;
        Optional<BranchesNode> branchesNodeOptional = branchesNode.getSubNodes()
                .stream()
                .filter(node -> node instanceof BranchesNode)
                .map(node -> (BranchesNode)node)
                .findAny();

        if(branchesNodeOptional.isPresent()) {
            nextBranchesStart = branchesNodeOptional.get().getStart();
            hasLeaves = false;
        }

        double radius = startPoint.getRadius();
        double start = branchesNode.getStart();

        double parentLength = parentElement.length() / start;

        double length = branchesNode.getLength().random(randomGenerator, seed) * (1f - start) * parentLength;
        double shape = branchesNode.getShape();
        double bendingStrength = branchesNode.getBendingStrength().random(randomGenerator, seed);
        double gravity = branchesNode.getGravity().random(randomGenerator, seed);
        int numberOfBends = branchesNode.getNumberOfBends().random(randomGenerator, seed);
        double roughness = branchesNode.getRoughness();

        List<Vector3> bends = generateBends(randomGenerator, seedGenerator);

        Vector3 position = startPoint.getPosition();
        Vector3 direction = new Vector3();

        int numberOfSteps = (int) floor(length / POINT_DISTANCE);
        double step = length;
        if (numberOfSteps == 0) {
            numberOfSteps = 1;
        } else {
            step = length / numberOfSteps;
        }

        List<TreeElementPoint> tmpPoints = new ArrayList<>();
        for (int i = 0; i <= numberOfSteps; i++) {
            double x = i * step / length;
            double currentRadius =  (1f - pow(i * step / length, shape)) * radius;
            direction = direction(bends, x, numberOfBends, bendingStrength, startDirection, gravity);
            direction.mult(step);
            position = add(position, direction);
            tmpPoints.add(new TreeElementPoint(position, direction, currentRadius, roughness, randomGenerator, tmpPoints.size()));
        }
        tmpPoints.add(new TreeElementPoint(position, direction, 0, roughness, randomGenerator, tmpPoints.size()));
        for (int i = 0; i < nextBranchesStart * (tmpPoints.size() - 1); i++) {
            points.add(tmpPoints.get(i));
        }
        if (points.size() == 0) {
            points.add(tmpPoints.get(0));
        }
        else if (nextBranchesStart < 0.999f) {
            points.add(approximatePoint(nextBranchesStart, tmpPoints));
        }
    }

    public void generateSideBranch(SideBranchesNode sideBranchesNode, double start, double rotation, TreeElement parentElement) {
        points.clear();

        hasLeaves = true;
        double nextBranchesStart = 1;

        Optional<BranchesNode> branchesNodeOptional = sideBranchesNode.getSubNodes()
                .stream()
                .filter(node -> node instanceof BranchesNode)
                .map(node -> (BranchesNode)node)
                .findAny();

        if(branchesNodeOptional.isPresent()) {
            nextBranchesStart = branchesNodeOptional.get().getStart();
            hasLeaves = false;
        }

        Vector3 startPosition = parentElement.getPosition(start);
        Vector3 parentDirection = parentElement.getDirection(start);
        parentDirection.normalize();
        double parentRadius = parentElement.getRadius(start);

        double radius = sideBranchesNode.getRadius().random(randomGenerator, seed) * parentRadius;
        double startDir = sideBranchesNode.getStartDirection();
        double shape = sideBranchesNode.getShape();
        double length = sideBranchesNode.getLength().random(randomGenerator, seed) * parentElement.length() * (1f - start);

        length += sideBranchesNode.getMinLength();

        double bendingStrength = sideBranchesNode.getBendingStrength().random(randomGenerator, seed);
        double gravity = sideBranchesNode.getGravity().random(randomGenerator, seed);
        int numberOfBends = sideBranchesNode.getNumberOfBends().random(randomGenerator, seed);
        double roughness = sideBranchesNode.getRoughness();

        int numberOfSteps = (int) floor(length / POINT_DISTANCE);
        double step = length;
        if (numberOfSteps == 0) {
            numberOfSteps = 1;
        } else {
            step = length / numberOfSteps;
        }

        Vector3 normal = parentDirection.anyNormal();
        normal.normalize();

        normal.normalize();
        Matrix rotMatrix = new Matrix();
        rotMatrix.rotate(toRadians(-startDir + 90), normal);

        Vector3 startDirection = Matrix.mult(parentDirection, rotMatrix);

        parentDirection.normalize();
        rotMatrix.rotate(toRadians(rotation), parentDirection);
        startDirection = Matrix.mult(startDirection, rotMatrix);

        startDirection.normalize();

        List<Vector3> bends = generateBends(randomGenerator, seedGenerator);
        List<TreeElementPoint> tmpPoints = new ArrayList<>();

        Vector3 direction = null;
        Vector3 position = startPosition;

        points.add(new TreeElementPoint(position, startDirection, radius, roughness, randomGenerator, points.size()));

        for (int i = 0; i <= numberOfSteps; i++) {
            double x = i * step / length;
            double currentRadius = (1f - pow(x, shape)) * radius;
            direction = direction(bends, x, numberOfBends, bendingStrength, startDirection, gravity);
            direction.mult(step);

            position = add(position, direction);
            tmpPoints.add(new TreeElementPoint(position, direction, currentRadius, roughness, randomGenerator, points.size()));

        }

        tmpPoints.add(new TreeElementPoint(position, direction, 0, roughness, randomGenerator, tmpPoints.size()));
        for (int i = 0;  i < nextBranchesStart * (tmpPoints.size() - 1); i++) {
            points.add(tmpPoints.get(i));
        }
        if (points.size() == 0)
            points.add(tmpPoints.get(0));
        else if (nextBranchesStart < 0.999f)
            points.add(approximatePoint(nextBranchesStart, tmpPoints));

    }

    public void generateRoot(RootsNode rootsNode, TreeElementPoint startPoint, Vector3 startDirection, Vector3 endDirection) {
        points.clear();

        double radius = startPoint.getRadius() * rootsNode.getRadius().random(randomGenerator, seed);
        double shape = rootsNode.getShape();
        double length = rootsNode.getLength().random(randomGenerator, seed);

        double bendingStrength = rootsNode.getBendingStrength().random(randomGenerator, seed);
        int numberOfBends = rootsNode.getNumberOfBends().random(randomGenerator, seed);
        double roughness = rootsNode.getRoughness();

        int numberOfSteps = (int) floor(length / POINT_DISTANCE);
        double step = length;
        if (numberOfSteps == 0) {
            numberOfSteps = 1;
        } else {
            step = length / numberOfSteps;
        }

        List<Vector3> bends = generateBends(randomGenerator, seedGenerator);

        points.add(startPoint);

        Vector3 position, direction, basicDirection;
        position = startPoint.getPosition();
        for (int i = 0; i <= numberOfSteps; i++) {
            double x = i * step / length;
            double currentRadius = (1f - pow(x, shape)) * radius;

            basicDirection = add(
                    mult(startDirection, 1 - x),
                    mult(endDirection, x));
            direction = direction(bends, x, numberOfBends, bendingStrength, basicDirection, 0);
            direction.mult(step);

            position = add(position, direction);
            points.add(new TreeElementPoint(position, direction.negative(), currentRadius, roughness, randomGenerator, points.size()));

        }
    }

    public Mesh generateMesh(double scaleU, double scaleV, LevelOfDetails levelOfDetails) {
        Mesh mesh = new Mesh();
        if (points.size() < 2) {
            return mesh;
        }

        int horizontalDivisions = levelOfDetails.getHorizontalDivisions();
        int verticalStep = levelOfDetails.getVerticalStep();

        double horizontalStep =  TreeElementPoint.ARRAY_SIZE / horizontalDivisions;
        double verticalDivisions =  (points.size() - 1) / verticalStep;
        List<TreeElementPoint> usedPoints = new ArrayList<>();
        double step =  1 / verticalDivisions;

        double x = 0;

        TreeElementPoint p;
        while (x < 1) {
            p = approximatePoint(x, points);
            usedPoints.add(p);
            x += step;
        }

        usedPoints.add(points.get(points.size() - 1));

        Vector3 position, normal, direction;
        double r;

        int[][] indices = new int[horizontalDivisions][usedPoints.size()];
        double[] textureCoordinateV = new double[usedPoints.size()];
        double currentTexCoordinateV = 0;
        Matrix matrix = new Matrix();

        for (int i = 0; i < usedPoints.size(); i++) {

            if (i > 0) {
                double distance = distance(i - 1, i, usedPoints);
                double radius = 1.0 / usedPoints.get(i - 1).getRadius() / 10;
                currentTexCoordinateV += radius * distance;
            } else
                currentTexCoordinateV += 0;
            textureCoordinateV[i] = currentTexCoordinateV;
            for (int j = 0; j < horizontalDivisions; j++) {

                r = usedPoints.get(i).getRadiusRandom((int) (j * horizontalStep));
                position = usedPoints.get(i).getPosition();
                direction = usedPoints.get(i).getDirection();

                matrix.rotate(toRadians( j / (double) horizontalDivisions * 360.0), direction);

                normal = usedPoints.get(i).getDirection().anyNormal();
                normal = Matrix.mult(normal, matrix);
                normal.normalize();

                normal.mult(r);
                position = add(position, normal);

                indices[j][i] = mesh.countVertices();

                normal.normalize();
                mesh.addVertex(new Vertex(position, normal));
            }
        }

        int v1, v2, v3;

        Triangle triangle;
        for (int i = 0; i < usedPoints.size() - 1; i++) {
            for (int j = 0; j < horizontalDivisions; j++) {
                if (j == horizontalDivisions - 1) {
                    v1 = indices[j][i];
                    v2 = indices[0][i];
                    v3 = indices[0][i + 1];
                    triangle = new Triangle(v1, v2, v3);

                    triangle.setTextureCoordinates(0, new TextureCoordinates( j / horizontalDivisions * scaleU, textureCoordinateV[i] * scaleV));
                    triangle.setTextureCoordinates(1, new TextureCoordinates(scaleU, textureCoordinateV[i] * scaleV));
                    triangle.setTextureCoordinates(2, new TextureCoordinates(scaleU, textureCoordinateV[i + 1] * scaleV));

                    mesh.addTriangle(triangle);

                    v1 = indices[0][i + 1];
                    v2 = indices[j][i + 1];
                    v3 = indices[j][i];

                    triangle = new Triangle(v1, v2, v3);

                    triangle.setTextureCoordinates(0, new TextureCoordinates(scaleU, textureCoordinateV[i + 1] * scaleV));
                    triangle.setTextureCoordinates(1, new TextureCoordinates( j / horizontalDivisions * scaleU, textureCoordinateV[i + 1] * scaleV));
                    triangle.setTextureCoordinates(2, new TextureCoordinates( j / horizontalDivisions * scaleU, textureCoordinateV[i] * scaleV));
                    mesh.addTriangle(triangle);
                } else {
                    v1 = indices[j][i];
                    v2 = indices[j + 1][i];
                    v3 = indices[j + 1][i + 1];
                    triangle = new Triangle(v1, v2, v3);

                    triangle.setTextureCoordinates(0, new TextureCoordinates( j / horizontalDivisions * scaleU, textureCoordinateV[i] * scaleV));
                    triangle.setTextureCoordinates(1, new TextureCoordinates( (j + 1) / horizontalDivisions * scaleU, textureCoordinateV[i] * scaleV));
                    triangle.setTextureCoordinates(2, new TextureCoordinates( (j + 1) / horizontalDivisions * scaleU, textureCoordinateV[i + 1] * scaleV));
                    mesh.addTriangle(triangle);

                    v1 = indices[j + 1][i + 1];
                    v2 = indices[j][i + 1];
                    v3 = indices[j][i];

                    triangle = new Triangle(v1, v2, v3);
                    triangle.setTextureCoordinates(0, new TextureCoordinates( (j + 1) / horizontalDivisions * scaleU, textureCoordinateV[i + 1] * scaleV));
                    triangle.setTextureCoordinates(1, new TextureCoordinates( j / horizontalDivisions * scaleU, textureCoordinateV[i + 1] * scaleV));
                    triangle.setTextureCoordinates(2, new TextureCoordinates( j / horizontalDivisions * scaleU, textureCoordinateV[i] * scaleV));

                    mesh.addTriangle(triangle);
                }
            }
        }

        return mesh;
    }

    public TreeElementPoint approximatePoint(int index) {
        return points.get(index);
    }

    public TreeElementPoint getLastPoint() {
        return points.get(points.size() - 1);
    }

    public boolean getHasLeaves() {
        return hasLeaves;
    }
}
