package pl.mj.treegen.core.tree.model.validation.node;

import pl.mj.treegen.core.tree.model.nodes.abstarct.Node;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ContainsValidator.class)
public @interface Contains {
    String message() default "Should contains not more than {maxCount} of {subNode}";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default {};
    int maxCount();
    Class<? extends Node> subNode();
}
