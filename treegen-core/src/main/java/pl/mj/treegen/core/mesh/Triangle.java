package pl.mj.treegen.core.mesh;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Mateusz Jaszewski on 2016-01-16.
 */
@Getter
@Setter
public class Triangle {
    private int[] indices;
    private TextureCoordinates[] textureCoordinatesArray;

    public Triangle(int v1, int v2, int v3) {
        indices = new int[3];
        indices[0] = v1;
        indices[1] = v2;
        indices[2] = v3;
        textureCoordinatesArray = new TextureCoordinates[3];
    }

    public void setTextureCoordinates(int index, TextureCoordinates textureCoordinates) {
        this.textureCoordinatesArray[index] = textureCoordinates;
    }

    public TextureCoordinates getTextureCoordinates(int index) {
        return textureCoordinatesArray[index];
    }

    public String toString() {
        return indices[0] + " " + indices[1] + " " + indices[2];
    }

    public int getVertexIndex(int index) {
        if (index >= 0 && index < 3) {
            return indices[index];
        }
        throw new IllegalArgumentException();
    }
}
