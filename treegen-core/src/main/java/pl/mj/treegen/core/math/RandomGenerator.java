package pl.mj.treegen.core.math;

import java.util.Random;

public class RandomGenerator {
    private int globalSeed;
    private int lastSeed;

    private Random globalRandom;
    private Random currentRandom;


    public RandomGenerator(int globalSeed) {
        this.globalSeed = globalSeed;
        this.globalRandom = new Random(globalSeed);
        lastSeed = globalRandom.nextInt();
        currentRandom = new Random(lastSeed);
    }

    public RandomGenerator() {
        globalSeed = 0;
    }

    public void setGlobalSeed(int seed) {
        globalSeed = seed;
    }

    public int getGlobalSeed() {
        return globalSeed;
    }

    public int randomInt(int min, int max, int seed) {

        if(seed != lastSeed) {
            currentRandom = new Random(seed);
        }

        max++;
        if (min == max) {
            return min;
        }
        int rand = globalRandom.nextInt() + currentRandom.nextInt();
        if (rand < 0) {
            rand = -rand;
        }
        return (rand % (max - min)) + min;

    }

    public double randomDouble(double min, double max, int seed) {
        double rand = randomInt(0, Integer.MAX_VALUE, seed) / (double)Integer.MAX_VALUE;
        return (rand * (max - min) + min);
    }

    private int hash(int a, int b) {
        return a * 31 + b;
    }

}
