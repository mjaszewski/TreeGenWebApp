package pl.mj.treegen.core.tree.model.range;

import pl.mj.treegen.core.math.RandomGenerator;

/**
 * Created by Mateusz Jaszewski on 2016-01-19.
 */
public abstract class Range <T extends Number & Comparable>{
    protected T min;
    protected T max;

    public Range(T min, T max) {
        if(min.compareTo(max) == 1) {
            throw new IllegalArgumentException("Inncorect range ! min should be less than max");
        }
        this.min = min;
        this.max = max;
    }

    public T getMin() {
        return min;
    }

    public T getMax() {
        return max;
    }

    public abstract T random(RandomGenerator randomGenerator, int seed);

    @Override
    public String toString() {
        return "(" + min.toString() + ", " + max.toString() + ")";
    }
}
