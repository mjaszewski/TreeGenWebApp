package pl.mj.treegen.core.tree.model.validation.between.integers;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Mateusz Jaszewski on 2016-01-24.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BetweenIntegersValidator.class)
public @interface BetweenIntegers {
    String message() default "Value ${validatedValue} out of range ({min}, {max})";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
    int min();
    int max();
}
