package pl.mj.treegen.core.tree.generator.leaves;

/**
 * Created by Mateusz Jaszewski on 2016-01-19.
 */
public enum LeavesType {
    SINGLE(1), DOUBLE(2), TRIPLE(3);

    private int numberOfPlanes;

    LeavesType(int numberOfPlanes) {
        this.numberOfPlanes = numberOfPlanes;
    }

    public int numberOfPlanes() {
        return numberOfPlanes;
    }
}
