package pl.mj.treegen.core.tree.model.nodes.implementations;

import lombok.Data;
import pl.mj.treegen.core.tree.model.nodes.abstarct.Node;
import pl.mj.treegen.core.tree.model.nodes.abstarct.NodeWithSubNodes;
import pl.mj.treegen.core.tree.model.range.RangeDouble;
import pl.mj.treegen.core.tree.model.range.RangeInteger;
import pl.mj.treegen.core.tree.model.validation.between.doubles.BetweenDoubles;
import pl.mj.treegen.core.tree.model.validation.between.integers.BetweenIntegers;
import pl.mj.treegen.core.tree.model.validation.node.Contains;
import pl.mj.treegen.core.tree.model.validation.node.SubNodes;

import java.util.ArrayList;
import java.util.List;

@Data
public class SideBranchesNode implements NodeWithSubNodes {

	@SubNodes({
			@Contains(subNode = TreeNode.class, maxCount = 0),
			@Contains(subNode = TrunkNode.class, maxCount = 0),
			@Contains(subNode = RootsNode.class, maxCount = 0),
			@Contains(subNode = LeavesNode.class, maxCount = 0),
			@Contains(subNode = BranchesNode.class, maxCount = 1)
	})
	private List<Node> subNodes = new ArrayList<>();

	@BetweenDoubles(min = 0, max = 1)
	private double shape = 0.5;

	@BetweenDoubles(min = 0, max = 1)
	private double start = 0.2;

	@BetweenDoubles(min = 0, max = 1)
	private double end = 0.9;

	@BetweenDoubles(min = -90, max = 90)
	private double startDirection = 0;

	@BetweenDoubles(min = 0, max = 1)
	private double roughness = 0.5;

	@BetweenDoubles(min = 0, max = 20)
	private double minLength = 5;

	@BetweenDoubles(min = 0, max = 2)
	private RangeDouble length = new RangeDouble(0.9, 1.1);

	@BetweenDoubles(min = 0, max = 1)
	private RangeDouble radius = new RangeDouble(0.4, 0.5);

	@BetweenIntegers(min = 1, max = 20)
	private RangeInteger number = new RangeInteger(9, 11);

	@BetweenDoubles(min = 0, max = 1)
	private RangeDouble bendingStrength = new RangeDouble(0.5, 1.0);

	@BetweenIntegers(min = 1, max = 10)
	private RangeInteger numberOfBends = new RangeInteger(4, 5);

	@BetweenDoubles(min = -1, max = 1)
	private RangeDouble gravity = new RangeDouble(-0.1, 0.1);

	@Override
	public List<Node> getSubNodes() {
		return subNodes;
	}

	@Override
	public void setSubNodes(List<Node> subNodes) {
		this.subNodes = subNodes;
	}
}
