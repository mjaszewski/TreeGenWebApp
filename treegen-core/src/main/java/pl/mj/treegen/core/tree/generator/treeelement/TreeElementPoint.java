package pl.mj.treegen.core.tree.generator.treeelement;

import lombok.Getter;
import lombok.Setter;
import pl.mj.treegen.core.math.RandomGenerator;
import pl.mj.treegen.core.math.Vector3;

@Setter
@Getter
public class TreeElementPoint implements Cloneable {
    public final static int ARRAY_SIZE = 120;
    private Vector3 position;
    private Vector3 direction;
    private double radius;
    private double[] radiusRandom;

    public TreeElementPoint() {
        radiusRandom = new double[ARRAY_SIZE];
    }

    public TreeElementPoint(Vector3 position, Vector3 direction, double radius, double roughness, RandomGenerator randomGenerator, int id) {
        this.radius = radius;
        this.direction = direction;
        this.direction.normalize();
        this.position = position;
        radiusRandom = new double[ARRAY_SIZE];

        for (int i = 0; i < ARRAY_SIZE; i++) {
            radiusRandom[i] = radius + radius * roughness * randomGenerator.randomDouble(-0.1, 0.1, i + id * id);
        }
    }

    public double getRadiusRandom(int index) {
        return radiusRandom[index];
    }

    public void setRadiusRandom(int index, double random) {
        radiusRandom[index] = random;
    }

    @Override
    public TreeElementPoint clone() {
        TreeElementPoint point = new TreeElementPoint();
        for (int i = 0; i < ARRAY_SIZE; i++) {
            point.radiusRandom[i] = radiusRandom[i];
        }
        point.direction = direction.clone();
        point.position = position.clone();
        point.radius = radius;
        return point;
    }
}
