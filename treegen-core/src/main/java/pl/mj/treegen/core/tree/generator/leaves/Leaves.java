package pl.mj.treegen.core.tree.generator.leaves;

import pl.mj.treegen.core.math.Matrix;
import pl.mj.treegen.core.math.RandomGenerator;
import pl.mj.treegen.core.math.SeedGenerator;
import pl.mj.treegen.core.math.Vector3;
import pl.mj.treegen.core.mesh.Mesh;
import pl.mj.treegen.core.mesh.TextureCoordinates;
import pl.mj.treegen.core.mesh.Triangle;
import pl.mj.treegen.core.mesh.Vertex;
import pl.mj.treegen.core.tree.model.nodes.implementations.LeavesNode;
import pl.mj.treegen.core.tree.generator.treeelement.TreeElementPoint;

import java.util.List;

import static java.lang.Math.*;
import static pl.mj.treegen.core.math.Vector3.*;

public abstract class Leaves {

    public static Mesh generateLeaves(LeavesNode leavesNode, List<TreeElementPoint> points,
                                      RandomGenerator randomGenerator, SeedGenerator seedGenerator) {

        Mesh mesh = new Mesh();
        for (int i = 0; i < points.size(); i++) {
            TreeElementPoint leavesCenter = points.get(i);

            double size = leavesNode.getSize().random(randomGenerator, seedGenerator.nextSeed());

            Vector3 up = leavesCenter.getDirection();
            up.normalize();

            Vector3 right = cross(up, new Vector3(0, 1, 0));
            right.normalize();

            double angle = randomGenerator.randomDouble(0, 360, seedGenerator.nextSeed());
            Matrix rotate = new Matrix();
            rotate.rotate(toRadians(angle), up);

            right = Matrix.mult(right, rotate);
            right.normalize();

            Vector3 forward = cross(up, right);
            forward.normalize();

            Vector3 x[] = new Vector3[3];
            Vector3 y[] = new Vector3[3];

            y[0] = up;
            x[0] = forward;

            y[1] = up;
            x[1] = right;

            y[2] = right;
            x[2] = forward;

            generatePlanes(leavesCenter, leavesNode.getLeavesType().numberOfPlanes(), size, mesh, x, y);
        }
        return mesh;
    }

    private static void generatePlanes(TreeElementPoint leavesCenter, int number, double size, Mesh mesh, Vector3[] x, Vector3[] y) {
        for (int i = 0; i < number; i++) {

            Triangle triangle;
            Vector3 position;
            Vector3 normal;

            position = add(leavesCenter.getPosition(), mult(y[i], size / 2));
            position = add(position, mult(x[i], size / 2));
            normal = sub(position, leavesCenter.getPosition());
            mesh.addVertex(new Vertex(position, normal));

            position = add(leavesCenter.getPosition(), mult(y[i].negative(), size / 2));
            position = add(position, mult(x[i], size / 2));
            normal = sub(position, leavesCenter.getPosition());
            mesh.addVertex(new Vertex(position, normal));

            position = add(leavesCenter.getPosition(), mult(y[i].negative(), size / 2));
            position = add(position, mult(x[i].negative(), size / 2));
            normal = sub(position, leavesCenter.getPosition());
            mesh.addVertex(new Vertex(position, normal));

            position = add(leavesCenter.getPosition(), mult(y[i], size / 2));
            position = add(position, mult(x[i].negative(), size / 2));
            normal = sub(position, leavesCenter.getPosition());
            mesh.addVertex(new Vertex(position, normal));

            triangle = new Triangle(mesh.countVertices() - 2, mesh.countVertices() - 4, mesh.countVertices() - 1);
            triangle.setTextureCoordinates(0, new TextureCoordinates(0, 0));
            triangle.setTextureCoordinates(1, new TextureCoordinates(1, 1));
            triangle.setTextureCoordinates(2, new TextureCoordinates(0, 1));
            mesh.addTriangle(triangle);

            triangle = new Triangle(mesh.countVertices() - 2, mesh.countVertices() - 3, mesh.countVertices() - 4);
            triangle.setTextureCoordinates(0, new TextureCoordinates(0, 0));
            triangle.setTextureCoordinates(1, new TextureCoordinates(1, 0));
            triangle.setTextureCoordinates(2, new TextureCoordinates(1, 1));
            mesh.addTriangle(triangle);
        }
    }
}
