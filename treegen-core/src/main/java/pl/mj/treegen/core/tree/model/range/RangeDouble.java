package pl.mj.treegen.core.tree.model.range;

import pl.mj.treegen.core.math.RandomGenerator;

/**
 * Created by Mateusz Jaszewski on 2016-01-19.
 */
public class RangeDouble extends Range<Double> {

    public RangeDouble(Double min, Double max) {
        super(min, max);
    }

    @Override
    public Double random(RandomGenerator randomGenerator, int seed) {
        return randomGenerator.randomDouble(min, max, seed);
    }
}
