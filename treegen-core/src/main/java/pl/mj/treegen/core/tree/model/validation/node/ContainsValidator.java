package pl.mj.treegen.core.tree.model.validation.node;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

/**
 * Created by Mateusz Jaszewski on 2016-01-24.
 */
public class ContainsValidator implements ConstraintValidator<Contains, Collection> {

    private double maxCount;
    private Class subNodeClass;

    @Override
    public void initialize(Contains constraintAnnotation) {
        maxCount = constraintAnnotation.maxCount();
        subNodeClass = constraintAnnotation.subNode();
    }

    @Override
    public boolean isValid(Collection value, ConstraintValidatorContext context) {
        return value.stream().filter(node -> subNodeClass.isInstance(node)).count() <= maxCount;
    }

}
