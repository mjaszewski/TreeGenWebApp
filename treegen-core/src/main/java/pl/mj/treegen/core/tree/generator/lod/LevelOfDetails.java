package pl.mj.treegen.core.tree.generator.lod;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import static java.util.Arrays.asList;

/**
 * Created by Mateusz Jaszewski on 2016-01-27.
 */

@Getter
@Setter
public class LevelOfDetails {

    public static List<LevelOfDetails> DEFAULT_LODS() {
        return asList(
                new LevelOfDetails(1, 12),
                new LevelOfDetails(2, 10),
                new LevelOfDetails(3, 8),
                new LevelOfDetails(5, 6),
                new LevelOfDetails(7, 4)
        );
    }

    private int verticalStep;
    private int horizontalDivisions;

    public LevelOfDetails(int verticalStep, int horizontalDivisions) {
        this.verticalStep = verticalStep;
        this.horizontalDivisions = horizontalDivisions;
    }
}
