package pl.mj.treegen.core.exporter.implementations;

import pl.mj.treegen.core.exporter.Exporter;
import pl.mj.treegen.core.mesh.Mesh;
import pl.mj.treegen.core.mesh.Triangle;
import pl.mj.treegen.core.mesh.Vertex;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Created by Mateusz Jaszewski on 2016-01-27.
 */
public class OBJExporter implements Exporter {

    public static final String NEW_LINE = "\r\n";

    public void export(Mesh treeMesh, Mesh leavesMesh, OutputStream outputStream) throws IOException {

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);

        writeMeshVertices(treeMesh, outputStreamWriter);
        writeMeshVertices(leavesMesh, outputStreamWriter);

        writeMeshNormals(treeMesh, outputStreamWriter);
        writeMeshNormals(leavesMesh, outputStreamWriter);

        writeMeshTextureCoordinates(treeMesh, outputStreamWriter);
        writeMeshTextureCoordinates(leavesMesh, outputStreamWriter);

        outputStreamWriter.write("g treeMesh\r\n");
        outputStreamWriter.write("usemtl treeMesh\r\n");
        writeMeshTriangles(treeMesh, outputStreamWriter, 0, 0);

        outputStreamWriter.write("g leavesMesh\r\n");
        outputStreamWriter.write("usemtl leavesMesh\r\n");
        writeMeshTriangles(leavesMesh, outputStreamWriter, treeMesh.getVertices().size(), treeMesh.getTriangles().size() * 3);

        outputStreamWriter.flush();
        outputStreamWriter.close();
    }

    private void writeMeshTextureCoordinates(Mesh mesh, OutputStreamWriter outputStreamWriter) throws IOException {
        for (Triangle triangle : mesh.getTriangles()) {
            for (int i = 0; i < 3; i++) {
                outputStreamWriter.write("vt " + triangle.getTextureCoordinates(i) + NEW_LINE);
            }
        }
    }

    private void writeMeshNormals(Mesh mesh, OutputStreamWriter outputStreamWriter) throws IOException {
        for (Vertex vertex : mesh.getVertices()) {
            outputStreamWriter.write("vn " + vertex.getNormal() + NEW_LINE);
        }
    }

    private void writeMeshVertices(Mesh mesh, OutputStreamWriter outputStreamWriter) throws IOException {
        for (Vertex vertex : mesh.getVertices()) {
            outputStreamWriter.write("v " + vertex.getPosition() + NEW_LINE);
        }
    }

    private void writeMeshTriangles(Mesh mesh, OutputStreamWriter outputStreamWriter,
                                    int vertexOffset, int textureCoordinateOffset) throws IOException {
        for (int i = 0; i < mesh.getTriangles().size(); i++) {
            Triangle triangle = mesh.getTriangles().get(i);
            outputStreamWriter.write("f ");
            for (int j = 0; j < 3; j++) {
                outputStreamWriter.write(vertexOffset + triangle.getIndices()[j] + 1 + "/");
                outputStreamWriter.write(textureCoordinateOffset + i * 3 + j + 1 + "/");
                outputStreamWriter.write(vertexOffset + triangle.getIndices()[j] + 1 + " ");
            }
            outputStreamWriter.write(NEW_LINE);
        }
    }
}
